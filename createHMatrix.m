function [A] = createHMatrix(x_var)
    
    for i=1:length(x_var.names)
       A.(sprintf(strcat(x_var.names{i})))=zeros(x_var.dim.(sprintf(x_var.names{i})), x_var.dim.(sprintf(x_var.names{i})));
    end
end