function [x_var_new]=CreateVariable(x_var, name, dimension, subdimensions, ctype, lb, ub)
%%%CreateVariable( current variable vector, variable name,       variable dimension,               {'number of dimensions' 'dimension 1' 'dimension 2' 'dimension3' 'dimension4'},       'variable type',    variable lower bound,  variable upper bound);
%% creates sturctured variable with all vectors
    x_var_new=x_var;
    x_var_new.dim.(sprintf(name))=dimension;
	x_var_new.subdim=[x_var.subdim; subdimensions];
    x_var_new.names=[x_var.names {name}];
    x_var_new.type=[x_var.type,   charvect(dimension, ctype)];

    x_var_new.ub=[x_var.ub; ones(dimension, 1)*ub];
    x_var_new.lb=[x_var.lb; ones(dimension, 1)*lb];
    x_var_new.dim.vect=[x_var.dim.vect; dimension];
    if length(x_var.dim.vect_end)>=1
        x_var_new.dim.vect_start=   [x_var.dim.vect_start;      x_var.dim.vect_end(length(x_var.dim.vect_end)) + 1];
        x_var_new.dim.vect_end=     [x_var.dim.vect_end;        x_var.dim.vect_end(length(x_var.dim.vect_end)) + dimension];
        x_var_new.details.(sprintf(name)).vect_start    =       x_var.dim.vect_end(length(x_var.dim.vect_end)) + 1;
        x_var_new.details.(sprintf(name)).vect_end      =       x_var.dim.vect_end(length(x_var.dim.vect_end)) + dimension;
    else
        x_var_new.dim.vect_start=   [x_var.dim.vect_start;   1];
        x_var_new.dim.vect_end=     [x_var.dim.vect_end; 	dimension];
        x_var_new.details.(sprintf(name)).vect_start=1;
        x_var_new.details.(sprintf(name)).vect_end=dimension;
    end
    x_var_new.dim.total=x_var.dim.total+dimension;
    
    x_var_new.details.(sprintf(name)).dim=dimension;
    x_var_new.details.(sprintf(name)).type=ctype;
    x_var_new.details.(sprintf(name)).lb=lb;
    x_var_new.details.(sprintf(name)).ub=ub;
    x_var_new.details.(sprintf(name)).name=name;
    x_var_new.details.(sprintf(name)).subdimensions=subdimensions;
    
    
     

end