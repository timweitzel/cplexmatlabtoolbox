function [A,b] = createMatrix(dim_constraint,x_var)
    
    for i=1:length(x_var.names)
       A.(sprintf(strcat(x_var.names{i})))=zeros(dim_constraint, x_var.dim.(sprintf(x_var.names{i})));
    end
    
    b=zeros(dim_constraint,1);
end