function PROBLEM=concatCPLEXinput(PROBLEM_input, A_sub, b, x_var, type)
%% Function concatenates input matrix Aeq/Aineq and input vector beq/bineq. Type can be defined by 'type'-input
PROBLEM=PROBLEM_input;
if strcmp(type,'eq')
    if isfield(PROBLEM, 'Aeq')
        PROBLEM.Aeq=[PROBLEM.Aeq;concatenateMatrix(x_var,A_sub)];
        PROBLEM.beq=[PROBLEM.beq;b];
    else
        
        PROBLEM.Aeq=concatenateMatrix(x_var,A_sub);
        PROBLEM.beq=b;
    end
else
    if isfield(PROBLEM, 'Aineq')
        PROBLEM.Aineq=[PROBLEM.Aineq;concatenateMatrix(x_var,A_sub)];
        PROBLEM.bineq=[PROBLEM.bineq;b];
    else
        PROBLEM.Aineq=concatenateMatrix(x_var,A_sub);
        PROBLEM.bineq=b;
    end
end

end